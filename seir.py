import numpy as np
from scipy.integrate import odeint
from typing import Optional


class NPI:
    """Class representing an NPI (non-pharmaceutical intervention), active from a given day, with given strength and fade-in time.


    The constructor take the following arguments:

    :param start: Time from which on the NPI is active (in days)
    :param end: Time when the NPI is stopped (in days)
    :param strength: Strength (= effectiveness) of the NPI
    :param fade: Fade-in time of the NPI (in days)
    """

    def __init__(
        self,
        start: float = np.inf,
        end: float = np.inf,
        strength: float = 0.0,
        fade: float = 1.0,
    ) -> None:
        """Constructor of NPI

        :param start: Time from which on the NPI is active (in days)
        :param end: Time when the NPI is stopped (in days)
        :param strength: Strength (= effectiveness) of the NPI
        :param fade: Fade-in time of the NPI (in days)

        :returns: Instance of NPI
        """
        self.time = start
        self.end = end
        self.strength = strength
        self.fade = fade

    def apply(self, t: int, transmission: float) -> float:
        """Adjust a given `transmission` according to the time `t` and the NPI

        :param t: Time in days
        :param transmission: Transmission value

        :returns: Adjusted transmission value
        """
        if t >= self.time and t <= self.end + self.fade:
            if self.fade != 0:
                fade = (t - self.time) / self.fade
                fade = min(fade, 1)
                if t > self.end:
                    fade = (self.fade - (t - self.end)) / self.fade
            else:
                fade = 1
            transmission *= 1 - self.strength * fade
        return transmission


class SEIR:
    """Class representing a SEIR (Susceptible-Exposed-Infected-Recovered) model.
    It can be used to model the evolution of a viral outbreak.
    The model is based on the number of susceptible (S), exposed (E), infected (I) and recovered (R) persons, and based on assumed transmission, recovery and transition rates.

    Mathematically the differential equations are as follows. N is the total number of people (N = S + E + I + R):

        dSdt = -1/N * transmission * S * I
        dEdt = 1/N * transmission * S * I - transition * E
        dIdt = transition * E - recovery * I
        dRdt = recovery * I


    The user can supply the initial number of people `N`, the number of exposed `E`, infected `I`. `R` is always 0 initially, and `S` is calculated accordingly.
    The user can supply the `transmission`, the `recovery` and the `transition` coefficients.
    The user can pass a list of NPIs to apply to attenuate the transmission.

    To solve the model, call the `solve` method, which will perform the integration. It returns an nd.array of S, E, I, R and the timesteps t.


    The constructor take the following arguments:

    :param N: Total number of people
    :param E: Number of exposed people
    :param I: Number of infected people
    :param days: Number of days to model
    :param transmission: Transmission rate
    :param recovery: Recovery rate
    :param recovery: Transition rate
    :param npis: List of NPIs to apply. Pass `None` (default) or an empty list to not apply any NPIs.
    """

    def __init__(
        self,
        N: int = 1_000,
        E: int = 0,
        I: int = 1,
        days: int = 160,
        transmission: float = 0.3,
        recovery: float = 0.1,
        transition: float = 0.3,
        npis: Optional[list[NPI]] = None,
        weekend: bool = False,
    ) -> None:
        """Constructor of SEIR

        :param N: Total number of people
        :param E: Number of exposed people
        :param I: Number of infected people
        :param days: Number of days to model
        :param transmission: Transmission rate
        :param recovery: Recovery rate
        :param recovery: Transition rate
        :param npis: List of NPIs to apply. Pass `None` (default) or an empty list to not apply any NPIs.
        """
        self.N = N
        self.E = E
        self.I = I
        self.R = 0
        self.S = self.N - self.E - self.I - self.R
        self.days = days
        self.transmission = transmission
        self.recovery = recovery
        self.transition = transition
        self.npis = npis
        self.weekend = weekend

    @staticmethod
    def __derive(
        y0: tuple,
        t: int,
        transmission: float,
        recovery: float,
        transition: float,
        npis: list[NPI],
        weekend=False,
    ) -> tuple[float]:
        """Model to integrate with odeint"""
        S, E, I, R = y0
        N = sum(y0)

        for npi in npis:
            transmission = npi.apply(t, transmission)

        if weekend:
            tt = t % 7
            if tt > 2 and tt < 5:
                I *= 0.8
            elif tt > 4:
                I *= 1.2

        dSdt = -1 / N * transmission * S * I
        dEdt = 1 / N * transmission * S * I - transition * E
        dIdt = transition * E - recovery * I
        dRdt = recovery * I

        return dSdt, dEdt, dIdt, dRdt

    def solve(self) -> tuple[np.ndarray, np.ndarray]:
        """Solve (integrate) the SEIR model.

        :returns: Tuple of the integration result. The first element contains a ndarray of S, E, I, R, the second element are the time steps.
        """
        t = np.arange(self.days, dtype=int)
        y0 = self.S, self.E, self.I, self.R
        if self.npis == None:
            self.npis = []
        args = (
            self.transmission,
            self.recovery,
            self.transition,
            self.npis,
            self.weekend,
        )
        res = odeint(SEIR.__derive, y0, t, args=args)
        return res.T, t
