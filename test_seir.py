from seir import SEIR, NPI
import numpy as np
import matplotlib.pyplot as plt

s = SEIR()
(S, E, I, R), t = s.solve()

day = 0
eff = 0.0
fade = 7
end = np.inf
masking = NPI(start=day, end=end, strength=eff, fade=fade)

NPI_str = f"NPI (Day: {day}, End: {end}, Eff.: {int(eff*100):}% Fade-In: {fade})"

s2 = SEIR(weekend=True)
(S2, E2, I2, R2), t = s2.solve()


f = plt.figure(figsize=(12, 8))
ax, ax2, ax3 = f.subplots(3)
ax.plot(t, I, label=f"No NPIs" + f", Total: {I.sum().astype(int)}")
ax.plot(t, I2, label=f"No NPIs (weekend effect)" + f", Total: {I2.sum().astype(int)}")
# ax.plot(t2, I2, label=NPI_str + f', Total: {I2.sum().astype(int)}')
# ax.plot(t3, I3, label=NPI_str2 + f', Total: {I3.sum().astype(int)}')
ax.set_xlabel("Days")
ax.set_ylabel("# Infected / day")
ax.grid()
ax.legend()


def Rt_fun(S, R):
    return -np.diff(S) / np.diff(R)


Rt = Rt_fun(S, R)
Rt2 = Rt_fun(S2, R2)

Rt = np.diff(I)
Rt2 = np.diff(I2)

ax2.plot(t[1:], Rt, label=f"No NPIs")
ax2.plot(t[1:], Rt2, label=f"No NPIs (weekend effect)")
# ax2.plot(t2[1:], Rt2, label=NPI_str)
# ax2.plot(t3[1:], Rt3, label=NPI_str2)
ax2.set_xlabel("Days")
ax2.set_ylabel("Rt")
ax2.grid()
ax2.legend()


RRt = np.diff(Rt)
RRt2 = np.diff(Rt2)

ax3.plot(t[1:-1], RRt, label=f"No NPIs")
ax3.plot(t[1:-1], RRt2, label=f"No NPIs (weekend effect)")
# ax2.plot(t2[1:], Rt2, label=NPI_str)
# ax2.plot(t3[1:], Rt3, label=NPI_str2)
ax3.set_xlabel("Days")
ax3.set_ylabel("Delta Rt")
ax3.grid()
ax3.legend()


plt.show()
